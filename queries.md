# Index
```
data
```

# ID
```
contextId = a474dc6b-1afd-45b6-bd74-9e83302fb85a
```

# Mapping
```
PUT data
{
  "mappings": {
    "properties": {
      "savedAt": {
        "type":   "date",
        "format": "yyyy-MM-dd HH:mm:ss.SSSZ"
      },
      "statuses.PENDING": {
        "type":   "date",
        "format": "yyyy-MM-dd HH:mm:ss.SSSZ"
      },
      "statuses.SAVING": {
        "type":   "date",
        "format": "yyyy-MM-dd HH:mm:ss.SSSZ"
      },
      "statuses.SUCCESS": {
        "type":   "date",
        "format": "yyyy-MM-dd HH:mm:ss.SSSZ"
      },
      "statuses.FAILED": {
        "type":   "date",
        "format": "yyyy-MM-dd HH:mm:ss.SSSZ"
      },
      "metadata": {
        "type": "nested",
        "properties": {
          "scenario": {
            "type":   "text",
            "analyzer": "keyword"
          }
        }
      }
    }
  }
}
```

# data-load domain object

- workstreams: should be part of a reindexing process each time a new workstream is added to this control set

```
POST data/_create/a474dc6b-1afd-45b6-bd74-9e83302fb85a
{
  "controlSet": {
    "uuid": "32770fec-f20b-4c53-9e9c-e49d7e3601c1",
    "name": "cmr_evaluation_rule"
  },
  "metadata": [
      {
        "scenario": "Int Base",
        "reportingDate": "20200331"
      },
      {
        "scenario": "Fed Base",
        "reportingDate": "20200331"
      }
  ],  
  "workstreams": ["ppnr", "rwa", "trc"], 
  "savedBy": "Raphael",
  "savedAt": "2022-04-18 19:09:27.527+0200",
  "statuses":{
    "PENDING":"2022-04-18 19:17:18.854+0200",
    "SAVING":"2022-04-18 19:18:18.861+0200",
    "SUCCESS":"2022-04-18 19:19:18.860+0200"
  },
  "approval": "L1"
}
```

# Queries

## For calculator's page filtering
1. matches reportingDate
2. has at least one of the scenarios, or doesn't have scenario at all
3. scores based on
   1. status
      1. SUCCESS
      2. SAVING
   2. approval
      1. L2
      2. L1
4. order by score, savedAt
```
GET data/_search
{
  "sort" : [
    "_score",
    { "savedAt" : "desc" }
  ],
  "query": {
    "bool": {
      "must": [
        {
          "nested": {
            "path": "metadata",
            "query": {
              "bool": {
                "must": [
                  {
                    "term": {
                      "metadata.reportingDate": {
                        "value": "20200331"
                      }
                    }
                  },
                  {
                    "bool": {
                      "should": [
                        {
                          "match": {
                            "metadata.scenario": "Int Base"
                          }
                        },
                        {
                          "match": {
                            "metadata.scenario": "Fed Base"
                          }
                        },
                        {
                          "bool": {
                            "must_not": {
                              "exists": {
                                "field": "metadata.scenario"
                              }
                            }
                          }
                        }
                      ],
                      "minimum_should_match": 1
                    }
                  }
                ]
              }
            }
          }
        }
      ],
      "should": [
        {
          "exists": {
             "field": "statuses.SUCCESS"
          }
        },
        {
          "exists": {
             "field": "statuses.SAVING"
          }
        },
        {
          "exists": {
             "field": "approval"
          }
        },
        {
          "term": {
            "approval": "L2"
          }
        },
        {
          "term": {
            "approval": "L1"
          }
        }
      ],
      "minimum_should_match": 0
    }
  }
}

```
