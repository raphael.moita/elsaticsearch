
POST data/_create/1bc20fec-1afd-45b6-bd74-9e83302fb85a
```json
{
  "controlSet": {
    "uuid": "32770fec-f20b-4c53-9e9c-e49d7e3601c1",
    "name": "cmr_evaluation_rule"
  },
  "metadata": [
      {
        "scenario": "Int Base",
        "reportingDate": "20200331"
      },
      {
        "scenario": "Fed Base",
        "reportingDate": "20200331"
      }
  ],  
  "workstreams": ["ppnr", "rwa", "trc"], 
  "savedBy": "Raphael Moita",
  "savedAt": "2022-04-18 19:09:27.527+0200",
  "statuses":{
    "PENDING":"2021-12-01 01:01:18.854+0200",
    "SAVING":"2021-12-01 01:02:18.861+0200",
    "SUCCESS":"2021-12-01 01:03:18.860+0200"
  }
}
```

POST data/_create/a474dc6b-1afd-45b6-bd74-9e83302fb85a
```json
{
  "controlSet": {
    "uuid": "32770fec-f20b-4c53-9e9c-e49d7e3601c1",
    "name": "cmr_evaluation_rule"
  },
  "metadata": [
      {
        "scenario": "Int Base",
        "reportingDate": "20200331"
      },
      {
        "scenario": "Fed Base",
        "reportingDate": "20200331"
      }
  ],  
  "workstreams": ["ppnr", "rwa", "trc"], 
  "savedBy": "Raphael Moita",
  "savedAt": "2022-04-18 19:09:27.527+0200",
  "statuses":{
    "PENDING":"2022-01-01 01:01:18.854+0200",
    "SAVING":"2022-01-01 01:02:18.861+0200",
    "SUCCESS":"2022-01-01 01:03:18.860+0200"
  },
  "approval": "L1"
}
```

POST data/_create/9e83302d-1afd-45b6-bd74-9e83302fb85a
```json
{
  "controlSet": {
    "uuid": "32770fec-f20b-4c53-9e9c-e49d7e3601c1",
    "name": "cmr_evaluation_rule"
  },
  "metadata": [
      {
        "scenario": "Int Base",
        "reportingDate": "20200331"
      },
      {
        "scenario": "Fed Base",
        "reportingDate": "20200331"
      }
  ],  
  "workstreams": ["ppnr", "rwa", "trc"], 
  "savedBy": "Raphael Moita",
  "savedAt": "2022-04-18 19:09:27.527+0200",
  "statuses":{
    "PENDING":"2022-01-02 01:01:18.854+0200",
    "SAVING":"2022-01-02 01:02:18.861+0200",
    "SUCCESS":"2022-01-02 01:03:18.860+0200"
  }
}
```

POST data/_create/ae45b62d-1afd-45b6-bd74-9e83302fb85a
```json
{
  "controlSet": {
    "uuid": "32770fec-f20b-4c53-9e9c-e49d7e3601c1",
    "name": "cmr_evaluation_rule"
  },
  "metadata": [
      {
        "scenario": "Int Base",
        "reportingDate": "20200331"
      },
      {
        "scenario": "Fed Base",
        "reportingDate": "20200331"
      }
  ],  
  "workstreams": ["ppnr", "rwa", "trc"], 
  "savedBy": "Raphael Moita",
  "savedAt": "2022-04-18 19:09:27.527+0200",
  "statuses":{
    "PENDING":"2022-04-18 19:17:18.854+0200",
    "SAVING":"2022-04-18 19:18:18.861+0200"
  }
}
```

POST data/_create/cc45b62d-1afd-45b6-bd74-9e83302fb85a
```json
{
  "controlSet": {
    "uuid": "32770fec-f20b-4c53-9e9c-e49d7e3601c1",
    "name": "cmr_evaluation_rule"
  },
  "metadata": [
      {
        "scenario": "Int Base",
        "reportingDate": "20200331"
      }
  ],  
  "workstreams": ["ppnr", "rwa", "trc"], 
  "savedBy": "Raphael Moita",
  "savedAt": "2022-04-18 19:09:27.527+0200",
  "statuses":{
    "PENDING":"2022-04-18 19:17:18.854+0200",
    "SAVING":"2022-04-18 19:18:18.861+0200"
  }
}
```

POST data/_create/aac2b22d-1afd-45b6-bd74-9e83302fb85a
```json
{
  "controlSet": {
    "uuid": "32770fec-f20b-4c53-9e9c-e49d7e3601c1",
    "name": "cmr_evaluation_rule"
  },
  "metadata": [
      {
        "modelId": "M-1234",
        "reportingDate": "20200331"
      }
  ],  
  "workstreams": ["ppnr", "rwa", "trc"], 
  "savedBy": "Raphael Moita",
  "savedAt": "2022-04-18 19:17:27.527+0200",
  "statuses":{
    "PENDING":"2022-04-18 19:17:18.854+0200",
    "SAVING":"2022-04-18 19:18:18.861+0200",
    "SUCCESS":"2022-04-18 19:19:18.854+0200"
  }
}
```
