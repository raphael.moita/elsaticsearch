#!/bin/bash

docker network create elastic

IMAGE=elasticsearch
CONTAINER=es-node01
TAG=8.1.2

docker stop ${CONTAINER} 2> /dev/null
docker rm ${CONTAINER} 2> /dev/null

docker pull docker.elastic.co/elasticsearch/${IMAGE}:${TAG}
docker run --name ${CONTAINER} -e "discovery.type=single-node" --net elastic -p 9200:9200 -p 9300:9300 -t docker.elastic.co/elasticsearch/${IMAGE}:${TAG}
