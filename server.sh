#!/bin/bash

docker stop es-node01 2> /dev/null
docker rm es-node01 2> /dev/null

docker stop kib-01 2> /dev/null
docker rm kib-01 2> /dev/null

docker network create elastic
docker pull docker.elastic.co/elasticsearch/elasticsearch:8.1.2
docker run -d --name es-node01 -e "discovery.type=single-node" --net elastic -p 9200:9200 -p 9300:9300 -t docker.elastic.co/elasticsearch/elasticsearch:8.1.2

docker pull docker.elastic.co/kibana/kibana:8.1.2
docker run -d --name kib-01 --net elastic -p 5601:5601 docker.elastic.co/kibana/kibana:8.1.2