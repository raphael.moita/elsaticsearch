#!/bin/bash

docker network create elastic

IMAGE=kibana
TAG=8.1.2
CONTAINER=kibana-01

docker stop ${CONTAINER} 2> /dev/null
docker rm ${CONTAINER} 2> /dev/null

docker pull ${IMAGE}:${TAG}
docker run --name ${CONTAINER} --network=elastic -p 5601:5601 docker.elastic.co/kibana/${IMAGE}:${TAG}