package org.moita.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.moita.model.DataLoad;

import java.util.List;

import static java.util.Arrays.asList;
import static org.moita.factory.DataLoadMetadataFactory.create;
import static org.moita.model.Status.PENDING;
import static org.moita.model.Status.SAVING;

@Slf4j
public class DataLoadGeneratorTest
{
    @Test
    public void generateData()
    {
        String contextId1 = "a474dc6b-1afd-45b6-bd74-9e83302fb85a";
        String contextId2 = "fa954db0-5ae6-4e2f-b2db-cf2bc6eeb2e2";
        String contextId3 = "e408ec54-ffa3-4110-895d-67d1dccf532d";
        String contextId4 = "bc9e0953-a99a-41eb-8327-16170a3e9a2e";
        String contextId5 = "a474dc6b-1afd-45b6-bd74-9e83302fb85a";
        String contextId6 = "f2bc6eeb-5ae6-4e2f-b2db-cf2bc6eeb2e2";
        String contextId7 = "dccf532d-ffa3-4110-895d-67d1dccf532d";
        String contextId8 = "f1dccff1-a99a-41eb-8327-16170a3e9a2e";

        DataLoad dataLoad = create(contextId1, PENDING, asList("reportingDate", "scenario"), List.of(asList("20200331", "scenario-a")));
        dataLoad.addStatus(SAVING);

        List<DataLoad> dataLoads = asList(
                dataLoad,
            create(contextId1, PENDING, asList("reportingDate", "scenario"), List.of(asList("20200331", "scenario-a"))),
            create(contextId2, PENDING, asList("reportingDate", "scenario"), List.of(asList("20200331", "scenario-b"))),
            create(contextId3, PENDING, asList("reportingDate", "scenario"), List.of(asList("20200331", "scenario-c"))),
            create(contextId4, PENDING, asList("reportingDate", "scenario"), List.of(asList("20200331", "scenario-a"), asList("20200630", "scenario-d"))),

            create(contextId5, PENDING, asList("reportingDate", "scenario"), List.of(asList("20200630", "scenario-a"))),
            create(contextId6, PENDING, asList("reportingDate", "scenario"), List.of(asList("20200630", "scenario-b"))),
            create(contextId7, PENDING, asList("reportingDate", "scenario"), List.of(asList("20200630", "scenario-c"))),
            create(contextId8, PENDING, asList("reportingDate", "scenario"), List.of(asList("20200630", "scenario-a"), asList("20200630", "scenario-d")))
        );

        Gson gson = new GsonBuilder()
                .create();
        System.out.println(gson.toJson(dataLoads));
    }
}