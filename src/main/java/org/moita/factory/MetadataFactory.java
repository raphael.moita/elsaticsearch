package org.moita.factory;

import org.moita.model.Metadata;

import java.util.List;

public class MetadataFactory
{
    public static Metadata create(List<String> keys, List<String> values)
    {
        Metadata metadata = Metadata.builder().build();
        for (int idx = 0; idx < keys.size(); idx++)
        {
            metadata.add(keys.get(idx), values.get(idx));
        }
        return metadata;
    }
}
