package org.moita.factory;

import org.moita.model.DataLoad;
import org.moita.model.Metadata;
import org.moita.model.Status;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.util.Objects.nonNull;

public class DataLoadMetadataFactory
{
    public static String now()
    {
        return OffsetDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSZ"));
    }

    public static DataLoad create(String contextId, Status status, List<String> keys, List<List<String>> valuesList,
                                  Status approval, String savedBy)
    {
        DataLoad.DataLoadBuilder builder = DataLoad.builder()
                .contextId(contextId)
                .statuses(new HashMap<>() {{ put(status, now()); }});

        if (nonNull(valuesList))
        {
            List<Metadata> metadataList = new ArrayList<>();

            valuesList.forEach(values ->
                    metadataList.add(MetadataFactory.create(keys, values))
            );

            builder.metadata(metadataList);
            builder.approval(approval);
            builder.savedBy(savedBy);
            builder.savedAt(now());
        }

        return builder.build();
    }

    public static DataLoad create(String contextId, Status status, List<String> keys, List<List<String>> valuesList)
    {
        return create(contextId, status, keys, valuesList, Status.APPROVED, "Raphael");
    }
}
