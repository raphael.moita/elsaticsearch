package org.moita.rest;

import io.swagger.annotations.ApiOperation;
import org.moita.model.DataLoad;
import org.moita.service.DataLoadMetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.moita.configuration.Swagger.METADATA;

@RestController(METADATA)
@RequestMapping("/metadata")
public class DataLoadMetadataRest
{
    private DataLoadMetadataService service;

    @Autowired
    public DataLoadMetadataRest(DataLoadMetadataService service)
    {
        this.service = service;
    }

    @GetMapping("find")
    @ApiOperation("Find all metadata")
    public Page<DataLoad> find()
    {
        return service.find("",0, 100);
    }
}
