package org.moita.service;

import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.moita.model.DataLoad;
import org.moita.model.Metadata;
import org.moita.repository.DataLoadMetadataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;

import static java.util.Arrays.asList;

@Service
public class DataLoadMetadataService
{
    private final RestHighLevelClient restHighLevelClient;
    private final ElasticsearchRestTemplate elasticsearchRestTemplate;
    private final DataLoadMetadataRepository repository;

    @Autowired
    public DataLoadMetadataService(RestHighLevelClient restHighLevelClient,
                                   ElasticsearchRestTemplate elasticsearchRestTemplate,
                                   DataLoadMetadataRepository repository)
    {
        this.restHighLevelClient = restHighLevelClient;
        this.elasticsearchRestTemplate = elasticsearchRestTemplate;
        this.repository = repository;
    }

    public DataLoad save(DataLoad metadata) {
        return repository.save(metadata);
    }

    public Iterable<DataLoad> save(Collection<DataLoad> metadata) {
        return repository.saveAll(metadata);
    }

    public Page<DataLoad> findAll(int page, int size)
    {
        Pageable pageable = PageRequest.of(page, size);
        return repository.findAll(pageable);
    }

    public Page<DataLoad> find(String criteria, int page, int size)
    {
        Pageable pageable = PageRequest.of(page, size);
        return repository.findAll(pageable);
    }

    public Optional<DataLoad> findById(String contextId)
    {
        return repository.findById(contextId);
    }

    public IndexResponse insert(String contextId) throws IOException
    {
        DataLoad dataLoad = DataLoad.builder()
                .contextId("aaaa")
                .metadata(asList(
                        Metadata.builder()
                        .data(new HashMap<>()
                                {{
                                    put("reportingDate", "20220521");
                                    put("scenario", "Int Base");
                                }})
                        .build(),
                        Metadata.builder()
                                .data(new HashMap<>()
                                {{
                                    put("reportingDate", "20220521");
                                    put("scenario", "Int Base");
                                }}
                        ).build()))
                .build();

        IndexRequest indexRequest = new IndexRequest(dataLoad.getContextId())
                .source(dataLoad);

        return restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
    }
}
