package org.moita.configuration;

import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.time.LocalDate;

public class Swagger
{
    public static final String METADATA = "METADATA";
    public static final String BASE_PACKAGE = "org.moita.rest";

    public static Docket docket()
    {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                .build()
                .tags(new Tag(METADATA, "Metadata"))
                .directModelSubstitute(LocalDate.class, String.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .useDefaultResponseMessages(false);
    }

    public static ApiInfo apiInfo()
    {
        return new ApiInfoBuilder()
                .title("Metadata searching")
                .description("Metadata searching")
                .build();
    }
}
