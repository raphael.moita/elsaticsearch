package org.moita.configuration;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@EnableElasticsearchRepositories(basePackages = "org.moita.repository")
@ComponentScan(basePackages = { "org.moita" })
public class ElasticsearchConfig
{
    @Bean
    public RestHighLevelClient client()
    {
        ClientConfiguration clientConfiguration = ClientConfiguration.builder()
                .connectedTo("localhost:9200")
                .build();

        return RestClients.create(clientConfiguration).rest();
    }

    @Bean
    public ElasticsearchOperations elasticsearchTemplate()
    {
        return new ElasticsearchRestTemplate(client());
    }

    @Bean
    public Docket docket()
    {
        return Swagger.docket();
    }
}