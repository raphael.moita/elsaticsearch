package org.moita;

import lombok.extern.log4j.Log4j2;
import org.moita.repository.DataLoadMetadataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Log4j2
public class ElasticsearchApplication
{
    public DataLoadMetadataRepository metadataRepository;

    @Autowired
    public ElasticsearchApplication(DataLoadMetadataRepository metadataRepository)
    {
        this.metadataRepository = metadataRepository;
    }

    public static void main(String[] args)
    {
        SpringApplication.run(ElasticsearchApplication.class, args);
    }
}
