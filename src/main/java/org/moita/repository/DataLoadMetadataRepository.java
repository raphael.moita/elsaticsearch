package org.moita.repository;

import org.moita.model.DataLoad;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Optional;

public interface DataLoadMetadataRepository extends ElasticsearchRepository<DataLoad, String>
{
    Optional<DataLoad> findByContextId(String contextId);
}