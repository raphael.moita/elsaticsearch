package org.moita.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.moita.factory.DataLoadMetadataFactory.now;

@Document(indexName = "data-load")
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataLoad
{
    @Id
    private String contextId;
    private List<Metadata> metadata = new ArrayList<>();
    private String savedBy;
//    @Field(type = FieldType.Date, format = DateFormat.basic_date_time, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private String savedAt;
    @Builder.Default
    private Map<Status, String> statuses = new HashMap<>();
    private Status approval;

    public void addStatus(Status status)
    {
        statuses.computeIfAbsent(status, s -> now());
    }
}
