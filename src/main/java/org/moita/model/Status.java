package org.moita.model;

public enum Status
{
    PENDING,
    SAVING,
    SUCCESS,

    APPROVED,
    REJECTED
}
