package org.moita.model;

import lombok.*;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@Builder
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class Metadata
{
    @Builder.Default
    private final Map<String, String> data = new HashMap<>();

    public void add(String key, String value)
    {
        data.put(key, value);
    }
}

