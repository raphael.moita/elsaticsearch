#!/bin/bash

curl -XPUT http://localhost:9200/dataload/metadata/1 -H "Content-Type: application/json" -d '
{
    "contextId": "5b902805-612b-4aff-81f4-e893ab2e7b0b",
    "savedAt": 2022-04-10T16:55:57.448603,
    "savedBy": "Raphael Moita",
    "status": "PENDING",
    "metadata": {
      "scenario": "scenario-a",
      "reportingDate": "20200331"
    }
}'

curl -XPUT http://localhost:9200/dataload/metadata/1 -H "Content-Type: application/json" -d '
{
    "contextId": "5b902805-612b-4aff-81f4-e893ab2e7b0b",
    "savedAt": 2022-04-10T16:55:57.448603,
    "savedBy": "Raphael Moita",
    "status": "SUCCESS",
    "approval": "L1",
    "metadata": {
      "scenario": "scenario-a",
      "reportingDate": "20200331"
    }
}'

curl -XPUT http://localhost:9200/dataload/metadata/1 -H "Content-Type: application/json" -d '
{
    "contextId": "ba202ca5-112b-3aff-81f3-aa1ab2e7b0a",
    "savedAt": 2022-04-10T16:55:57.448603,
    "savedBy": "Raphael Moita",
    "status": "PENDING",
    "metadata": {
      "scenario": "scenario-b",
      "reportingDate": "20200331"
    }
}'

curl -XPUT http://localhost:9200/dataload/metadata/1 -H "Content-Type: application/json" -d '
{
    "contextId": "ba202ca5-112b-3aff-81f3-aa1ab2e7b0a",
    "savedAt": 2022-04-10T16:55:57.448603,
    "savedBy": "Raphael Moita",
    "status": "SUCCESS",
    "approval": "L1",
    "metadata": {
      "scenario": "scenario-b",
      "reportingDate": "20200331"
    }
}'

curl -XGET http://localhost:9200/dataload/metadata/1 -H "Content-Type: application/json" -d '
{
    "query": {
        "match": {
             "scenario": "scenario-d"
        }
    }
}'

curl -XGET http://localhost:9200/data-load/_search?pretty=true
